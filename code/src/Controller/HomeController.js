
const fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";



exports.index = function(req, res) {
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        console.log("Database created!");
        db.close();
    });
    res.send('Welcome Home im Index')
}

exports.index2 = function(req, res) {
    fs.readFile('views/index.html', function (err, html) {
        res.writeHeader(200, {"Content-Type": "text/html"});
        res.write(html);
        res.end();
    });
}
exports.style = function(req, res){
    res.sendFile('styles.css', {root: '/app/views/css'})
}

exports.sharks = function(req, res) {
    //res.send('sharks')
    res.sendFile('/sharks.html',{root: '/app/views'})
}