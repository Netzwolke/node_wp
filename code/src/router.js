const router = require("express").Router();
const express = require("express")
const homeController = require(__dirname + '/Controller/HomeController');

// Put all your server routes in here

router.use(function(req, res, next){
    console.log('Time: ', Date.now())
    next()
})
router.use('/', express.static( 'views'))


router.get('/', function (req, res){
    res.redirect('/app')
})
router.get('/app', homeController.index2)
router.get('/sharks', homeController.sharks)
router.get('/db', homeController.index)
//router.get('/css/styles.css', homeController.style)





module.exports = router;