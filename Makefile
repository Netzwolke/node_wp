PHP = php
NPM = npm
DOCKER = docker-compose
RUN = $(DOCKER) up -d nd_node
STOP = $(DOCKER) stop nd_node
EXEC = $(DOCKER) exec -d nd_node

install:
	$(RUN)
	$(EXEC) $(NPM) install


start:
	$(RUN)
	$(EXEC) npm start

stop:
	$(STOP)
	$(DOCKER) rm -f nd_node